﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dominion.Models
{
    public class Host
    {
        public string IpAddress { get; set; }
        public string HostName { get; set; }
        public string PingStatus { get; set; }

        public Host()
        {
            this.IpAddress = "";
            this.HostName = "";
            this.PingStatus = "";
        }
    }
}
