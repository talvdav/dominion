﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.NetworkInformation;
using ReactiveUI;

using Dominion.Models;

namespace Dominion.ViewModels
{
    public class HostListViewModel : ViewModelBase
    {
        public HostListViewModel()
        {
            Items = new ObservableCollection<Host>();
            IpTemplate = "10.0.0.0/24";
        }

        public ObservableCollection<Host> Items
        {
            get;
            set;
        }
        private string ipTemplate = String.Empty;

        public string IpTemplate
        {
            get => ipTemplate;
            set => this.RaiseAndSetIfChanged(ref ipTemplate, value);
        }
        public void CreateList()
        {
            var l = new HostList(IpTemplate);
            var count = 0;
            if (Items.Count == 0)
            {
                foreach (Host host in l)
                {
                    IPAddress ip = IPAddress.Parse(host.IpAddress);
                    try
                    {
                        //host.HostName = Dns.GetHostEntry(ip).HostName;
                        host.HostName = "hostname";
                    }
                    catch
                    {
                        host.HostName = "---";
                    }
                    try
                    {
                        //var p = new Ping();
                        //var r = p.Send(ip, 1000).Status;
                        //host.PingStatus = r.ToString();
                        host.PingStatus = "ping";
                    }
                    catch
                    {
                        host.PingStatus = "error";
                    }
                    Items.Add(host);
                    count++;
                }
            }
        }
        public ObservableCollection<Host> ProcessAll()
        {
            foreach (Host host in Items)
            {
                IPAddress ip = IPAddress.Parse(host.IpAddress);
                var rand = new Random();
                host.HostName = rand.Next(101).ToString();
                this.RaisePropertyChanged(host.HostName);
                host.PingStatus = rand.Next(101).ToString();
                this.RaisePropertyChanged(host.PingStatus);
            }
            return (Items);
        }


    }
}
