﻿using System;
using System.Collections.Generic;
using System.Text;

using ReactiveUI;


namespace Dominion.ViewModels
{

    public class MainWindowViewModel : ViewModelBase
    {
        ViewModelBase content;
        public MainWindowViewModel()
        {
            Content = List = new HostListViewModel();
        }

        public ViewModelBase Content
        {
            get => content;
            private set => this.RaiseAndSetIfChanged(ref content, value);
        }

        public HostListViewModel List { get; }
    }
}
