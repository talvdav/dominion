using System.Threading;

namespace Dominion.Sid {
    public class Sid{
        
        public static string Wait(int Duration) {

            Thread.Sleep(Duration);
            return (Duration.ToString()); 

        }
    }
}